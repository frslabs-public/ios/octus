
Pod::Spec.new do |s|
  s.name             = 'Octus'
  s.version          = '1.6.8'
  s.summary          = 'Scan Documents'
  s.homepage         = 'https://gitlab.com/frslabs-public/ios/octus'
  s.license          = 'MIT'
  s.author           = { 'sravani' => 'shravani@frslabs.com' }
  s.source           = { :http => 'https://octus-ios.repo.frslabs.space/octus-ios/1.6.8/Octus.framework.zip'}
  s.platform         = :ios
  s.ios.deployment_target = '12.0'
  s.ios.vendored_frameworks = 'Octus.framework'
  s.swift_version = '5.0'
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'}
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'}
end
