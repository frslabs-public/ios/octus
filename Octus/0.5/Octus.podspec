
Pod::Spec.new do |s|
  s.name             = 'Octus'
  s.version          = '0.5'
  s.summary          = 'A short description of Octus.'
  s.homepage         = 'https://gitlab.com/frslabs-public/ios/octussdk'
  s.license          = 'MIT'
  s.author           = { 'FRSLabs_Sravani' => 'shravani@frslabs.com' }
  s.source           = { :http => 'https://www.repo2.frslabs.space/repository/octus-ios/octus-ios/0.5/Octus.framework.zip'}
 s.platform         = :ios
  s.ios.deployment_target = '12.0'
s.ios.vendored_frameworks = 'Octus.framework'
 s.swift_version = '5.0'
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'}
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'}

end