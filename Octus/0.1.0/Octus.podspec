#
# Be sure to run `pod lib lint Octus.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Octus'
  s.version          = '0.1.0'
  s.summary          = 'Scan Documents'
  s.homepage         = 'https://gitlab.com/frslabs-public/ios/octus'
  s.license          = 'MIT'
  s.author           = { 'FRSLabs_Sravani' => 'shravani@frslabs.com' }
  s.source           = { :http => 'https://www.repo2.frslabs.space/repository/octus-ios/octus-ios/0.1.0/Octus.framework.zip'}
  s.swift_version = '5.0'
  s.ios.deployment_target = '12.0'
  s.ios.vendored_frameworks = 'Octus.framework'
  #s.dependency 'TesseractOCRiOS', '5.0.1'
  #s.dependency 'TensorFlowLiteSwift', '2.6.0'
  s.static_framework = true
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'}
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'}
  #s.source_files = "Octus/Classes/**/*.{swift,h,m}"
  #s.resources = "Octus/Assets/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"
  #s.resource_bundles = { 'Octus' => ['Octus/Assets/*.png', 'Octus/Assets/*.xcassets'] }

end

