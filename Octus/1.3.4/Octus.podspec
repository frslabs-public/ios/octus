
Pod::Spec.new do |s|
  s.name             = 'Octus'
  s.version          =  '1.3.4'
  s.summary          = 'Scan Documents'
  s.homepage         = 'https://gitlab.com/frslabs-public/ios/octus'
  s.license          = 'MIT'
  s.author           = { 'ashish' => 'ashish@frslabs.com' }
  s.source           = { :http => 'https://octus-ios.repo.frslabs.space/octus-ios/1.3.4/Octus.framework.zip'}
  s.platform         = :ios
  s.ios.deployment_target = '12.0'
  s.ios.vendored_frameworks = 'Octus.framework'
  s.swift_version = '5.0'
end
